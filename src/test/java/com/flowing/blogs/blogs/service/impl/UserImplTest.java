package com.flowing.blogs.blogs.service.impl;

import com.flowing.blogs.blogs.domain.Rol;
import com.flowing.blogs.blogs.domain.User;
import com.flowing.blogs.blogs.domain.enums.ERol;
import com.flowing.blogs.blogs.repository.IUserRepo;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
class UserImplTest {

    @Mock
    private IUserRepo userRepoMock;

    @InjectMocks
    private UserImpl userServiceMock;

    @Test
    void listUserTest() throws Exception {
        Rol rol1 = new Rol();
        rol1.setIdRol(1);
        rol1.setName(ERol.USER);
        rol1.setDescription("esta es una prueba");
        Rol rol2 = new Rol();
        rol2.setIdRol(1);
        rol2.setName(ERol.USER);
        rol2.setDescription("esta es una prueba");
        List<Rol> roles = new ArrayList<>();
        roles.add(rol1);
        roles.add(rol2);
        User user = new User();
        user.setIdUser(1);
        user.setUserName("Prueba");
        user.setFirstName("Prueba");
        user.setLastName("Prueba");
        user.setPassword("Prueba");
        user.setEmail("prueba@gmail.com");
        user.setEnabled(true);
        user.setCellPhone(4849455);
        user.setRoles(roles);
        List<User> users = new ArrayList<>();
        users.add(user);
        Mockito.when(userRepoMock.findAll()).thenReturn(users);
        List<User> userList = userServiceMock.toList();
        assertEquals(1, userList.size());
    }

    @Test
    void listUsersForIdTest() throws Exception {
        Rol rol1 = new Rol();
        rol1.setIdRol(1);
        rol1.setName(ERol.USER);
        rol1.setDescription("esta es una prueba");
        List<Rol> roles = new ArrayList<>();
        roles.add(rol1);
        User user = new User();
        user.setIdUser(1);
        user.setUserName("Prueba");
        user.setFirstName("Prueba");
        user.setLastName("Prueba");
        user.setPassword("Prueba");
        user.setEmail("prueba@gmail.com");
        user.setEnabled(true);
        user.setCellPhone(4849455);
        user.setRoles(roles);
        Mockito.when(userRepoMock.findById(1)).thenReturn(Optional.of(user));
        User userFound =  userServiceMock.toListForId(1);
        assertEquals(1, userFound.getIdUser());
        assertEquals(1, userFound.getRoles().size());
    }

    @Test
    void saveUserTest() throws Exception {
        Rol rol1 = new Rol();
        rol1.setIdRol(1);
        rol1.setName(ERol.USER);
        rol1.setDescription("esta es una prueba");
        List<Rol> roles = new ArrayList<>();
        roles.add(rol1);
        User user = new User();
        user.setIdUser(1);
        user.setUserName("Prueba");
        user.setFirstName("Prueba");
        user.setLastName("Prueba");
        user.setPassword("Prueba");
        user.setEmail("prueba@gmail.com");
        user.setEnabled(true);
        user.setCellPhone(4849455);
        user.setRoles(roles);
        Mockito.when(userRepoMock.save(any(User.class))).thenReturn(user);
        assertNotNull(userServiceMock.toRegister(new User()));
    }

    @Test
    void updateUserTest() throws Exception {
        Rol rol1 = new Rol();
        rol1.setIdRol(1);
        rol1.setName(ERol.USER);
        rol1.setDescription("esta es una prueba");
        List<Rol> roles = new ArrayList<>();
        roles.add(rol1);
        User user = new User();
        user.setIdUser(1);
        user.setUserName("Prueba");
        user.setFirstName("Prueba");
        user.setLastName("Prueba");
        user.setPassword("Prueba");
        user.setEmail("prueba@gmail.com");
        user.setEnabled(true);
        user.setCellPhone(4849455);
        user.setRoles(roles);

        User user1 = new User();
        user1.setIdUser(1);
        user1.setUserName("PruebaPrueba");
        user1.setFirstName("Prueba");
        user1.setLastName("Prueba");
        user1.setPassword("Prueba");
        user1.setEmail("pruebapruebita@gmail.com");
        user1.setEnabled(true);
        user1.setCellPhone(4849455);
        user1.setRoles(roles);
        Mockito.when(userRepoMock.save(any(User.class))).thenReturn(user);
        User userCreate = userServiceMock.toUpdate(user1);
        assertNotEquals("PruebaPrueba", userCreate.getUserName());
    }
}