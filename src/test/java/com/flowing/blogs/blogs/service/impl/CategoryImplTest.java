package com.flowing.blogs.blogs.service.impl;

import com.flowing.blogs.blogs.domain.Category;
import com.flowing.blogs.blogs.repository.ICategoryRepo;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;


import static org.mockito.ArgumentMatchers.any;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(MockitoExtension.class)
class CategoryImplTest {

    @Mock
    private ICategoryRepo categoryRepoMock;

    @InjectMocks
    private CategoryImpl categoryServiceMock;

    @Test
    void listCategoriesTest () throws Exception {
        Category category1 = new Category();
        category1.setIdCategory(1);
        category1.setName("Prueba 1");
        category1.setDescription("esta es la prueba 1");
        Category category2 = new Category();
        category2.setIdCategory(2);
        category2.setName("Prueba 2");
        category2.setDescription("esta es la prueba 2");
        Category category3 = new Category();
        category3.setIdCategory(3);
        category3.setName("Prueba 3");
        category3.setDescription("esta es la prueba 3");
        List<Category> categories = new ArrayList<>();
        categories.add(category1);
        categories.add(category2);
        categories.add(category3);
        Mockito.when(categoryRepoMock.findAll()).thenReturn(categories);
        List<Category> categoryList = categoryServiceMock.toList();
        assertEquals(0, categoryList.size());
    }

    @Test
    void listCategoriesForIdTest() throws Exception {
        Category category1 = new Category();
        category1.setIdCategory(1);
        category1.setName("Prueba 1");
        category1.setDescription("esta es la prueba 1");
        Mockito.when(categoryRepoMock.findById(1)).thenReturn(Optional.of(category1));
        Category category =  categoryServiceMock.toListForId(1);
        assertEquals(1, category.getIdCategory());
    }

    @Test
    void saveCategoryTest() throws Exception {
        Category category1 = new Category();
        category1.setIdCategory(1);
        category1.setName("Prueba 1");
        category1.setDescription("esta es la prueba 1");
        Mockito.when(categoryRepoMock.save(any(Category.class))).thenReturn(category1);
        assertNotNull(categoryServiceMock.toRegister(new Category()));
    }

    @Test
    void updateCategoryTest() throws Exception {
        Category category1 = new Category();
        category1.setIdCategory(1);
        category1.setName("Prueba 1");
        category1.setDescription("esta es la prueba 1");
        Category category2 = new Category();
        category2.setIdCategory(1);
        category2.setName("Prueba 2");
        Mockito.when(categoryRepoMock.save(any(Category.class))).thenReturn(category2);
        Category cat = categoryServiceMock.toUpdate(category2);
        assertNotEquals("Prueba 2", cat.getDescription());
        assertEquals("Prueba 2", cat.getName());
    }
}