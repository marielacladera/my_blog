package com.flowing.blogs.blogs.service.impl;

import com.flowing.blogs.blogs.domain.Blog;
import com.flowing.blogs.blogs.domain.Category;
import com.flowing.blogs.blogs.domain.Rol;
import com.flowing.blogs.blogs.domain.User;
import com.flowing.blogs.blogs.domain.enums.ERol;
import com.flowing.blogs.blogs.repository.IBlogRepo;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class BlogImplTest {

    @Mock
    private IBlogRepo blockRepoMock;

    @InjectMocks
    private BlogImpl BlogServiceMock;

    @Test
    void listForUserTest() {
        Rol rol1 = new Rol();
        rol1.setIdRol(1);
        rol1.setName(ERol.USER);
        rol1.setDescription("esta es una prueba");
        Rol rol2 = new Rol();
        rol2.setIdRol(1);
        rol2.setName(ERol.USER);
        rol2.setDescription("esta es una prueba");
        List<Rol> roles = new ArrayList<>();
        roles.add(rol1);
        roles.add(rol2);
        Category category = new Category();
        category.setIdCategory(1);
        category.setName("hello");
        User user = new User();
        user.setIdUser(1);
        user.setUserName("Prueba");
        user.setFirstName("Prueba");
        user.setLastName("Prueba");
        user.setPassword("Prueba");
        user.setEmail("prueba@gmail.com");
        user.setEnabled(true);
        user.setCellPhone(4849455);
        user.setRoles(roles);
        Blog blog= new Blog();
        blog.setIdBlog(1);
        blog.setUser(user);
        blog.setPublicationDate(LocalDate.now());
        blog.setTitle("prueba prueba");
        blog.setNameImage("prueba.JPG");
        blog.setNameFile("img.jpg");
        blog.setDescription("esta es una prueba");
        blog.setCategory(category);
        Blog blog2= new Blog();
        blog2.setIdBlog(2);
        blog2.setUser(user);
        blog2.setPublicationDate(LocalDate.now());
        blog2.setTitle("prueba prueba");
        blog2.setNameImage("prueba.JPG");
        blog2.setNameFile("img.jpg");
        blog2.setDescription("esta es una prueba");
        blog2.setCategory(category);
        List<Blog> blogs = new ArrayList<>();
        blogs.add(blog);
        blogs.add(blog2);
        Mockito.when(blockRepoMock.searchByUser(null, "Prueba2" )).thenReturn(blogs);
        List<Blog> blogList = BlogServiceMock.searchByUser(null, "Prueba2");
        assertEquals(2, blogList.size());
    }
}