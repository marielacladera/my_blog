package com.flowing.blogs.blogs.service.impl;

import com.flowing.blogs.blogs.domain.Rol;
import com.flowing.blogs.blogs.domain.enums.ERol;
import com.flowing.blogs.blogs.repository.IRolRepo;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;


import static org.mockito.ArgumentMatchers.any;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class RolImplTest {
    @Mock
    private IRolRepo rolRepoMock;

    @InjectMocks
    private RolImpl rolServiceMock;

    @Test
    void listRolTest() throws Exception {
        Rol rol1 = new Rol();
        rol1.setIdRol(1);
        rol1.setName(ERol.USER);
        rol1.setDescription("esta es una prueba");
        Rol rol2 = new Rol();
        rol2.setIdRol(1);
        rol2.setName(ERol.USER);
        rol2.setDescription("esta es una prueba");
        List<Rol> roles = new ArrayList<>();
        roles.add(rol1);
        roles.add(rol2);
        Mockito.when(rolRepoMock.findAll()).thenReturn(roles);
        List<Rol> listRol = rolServiceMock.toList();
        assertEquals(2, listRol.size());

    }

    @Test
    void listRolForIdTest() throws Exception {
        Rol rol1 = new Rol();
        rol1.setIdRol(1);
        rol1.setName(ERol.USER);
        rol1.setDescription("esta es una prueba");
        Mockito.when(rolRepoMock.findById(3)).thenReturn(Optional.of(rol1));
        Rol rol = rolServiceMock.toListForId(3);
        assertNotEquals(null, rol.getIdRol());

    }

    @Test
    void saveRolTest() throws  Exception {
        Rol rol1 = new Rol();
        rol1.setIdRol(1);
        rol1.setName(ERol.USER);
        rol1.setDescription("esta es una prueba");
        Mockito.when(rolRepoMock.save(any(Rol.class))).thenReturn(rol1);
        assertNotNull(rolServiceMock.toRegister(new Rol()));
    }

    @Test
    void updateRolTest() throws Exception {
        Rol rol1 = new Rol();
        rol1.setIdRol(1);
        rol1.setName(ERol.USER);
        rol1.setDescription("esta es una prueba");
        Rol rol2 = new Rol();
        rol2.setIdRol(1);
        rol2.setName(ERol.BDA);
        Mockito.when(rolRepoMock.save(any(Rol.class))).thenReturn(rol1);
        Rol rol = rolServiceMock.toUpdate(rol2);
        assertNotEquals("USER", rol.getName());
    }
}