package com.flowing.blogs.blogs.repository;

import com.flowing.blogs.blogs.domain.User;

public interface IUserRepo extends IGenericRepo<User, Integer>{
    User findOneByUserName(String userName);
}
