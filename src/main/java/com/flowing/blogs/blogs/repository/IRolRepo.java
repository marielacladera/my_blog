package com.flowing.blogs.blogs.repository;

import com.flowing.blogs.blogs.domain.Rol;

public interface IRolRepo extends IGenericRepo<Rol, Integer>{

}
