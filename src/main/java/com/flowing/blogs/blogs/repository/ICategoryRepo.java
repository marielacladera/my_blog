package com.flowing.blogs.blogs.repository;


import com.flowing.blogs.blogs.domain.Category;

public interface ICategoryRepo extends IGenericRepo<Category, Integer>{

}
