package com.flowing.blogs.blogs.repository;

import com.flowing.blogs.blogs.domain.Blog;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

public interface IBlogRepo extends IGenericRepo<Blog, Integer> {

    @Query("FROM Blog b WHERE b.user.idUser = :idUser OR LOWER(b.user.userName) = :userName")
    List<Blog> searchByUser(@Param("idUser") Integer idUser, @Param("userName") String userName);

    @Query("FROM Blog b WHERE b.category.idCategory = :idCat")
    List<Blog> searchByCategory(@Param("idCat") Integer idCat);

    @Query("FROM Blog b WHERE b.publicationDate BETWEEN :startDate AND :endDate")
    List<Blog> searchByDate(@Param("startDate") LocalDate startDate, @Param("endDate") LocalDate endDate);

}
