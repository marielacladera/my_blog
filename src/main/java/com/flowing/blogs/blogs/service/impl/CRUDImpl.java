package com.flowing.blogs.blogs.service.impl;

import com.flowing.blogs.blogs.service.ICRUD;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public abstract class CRUDImpl<T, ID> implements ICRUD<T, ID> {
    protected abstract JpaRepository<T, ID> getRepo();

    @Override
    public T toRegister(T t) throws Exception {
        return getRepo().save(t);
    }

    @Override
    public List<T> toList() throws Exception {
        return getRepo().findAll();
    }

    @Override
    public T toListForId(ID id) throws Exception {
        return getRepo().findById(id).orElse(null);
    }

    @Override
    public T toUpdate(T t) throws Exception{
        return getRepo().save(t);
    }

    @Override
    public void toDelete(ID id) throws Exception{
        getRepo().deleteById(id);
    }
}
