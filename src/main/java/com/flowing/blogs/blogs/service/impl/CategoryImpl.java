package com.flowing.blogs.blogs.service.impl;

import com.flowing.blogs.blogs.domain.Category;
import com.flowing.blogs.blogs.repository.ICategoryRepo;
import com.flowing.blogs.blogs.repository.IGenericRepo;
import com.flowing.blogs.blogs.service.ICategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CategoryImpl extends CRUDImpl<Category, Integer> implements ICategoryService {
    @Autowired
    private ICategoryRepo repo;

    @Override
    protected IGenericRepo<Category, Integer> getRepo() {
        return repo;
    }
}
