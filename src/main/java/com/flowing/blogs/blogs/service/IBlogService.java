package com.flowing.blogs.blogs.service;

import com.flowing.blogs.blogs.domain.Blog;

import java.time.LocalDate;
import java.util.List;

public interface IBlogService extends ICRUD<Blog, Integer> {
    List<Blog> searchByUser(Integer idUser, String userName);
    List<Blog> searchByCategory(Integer idCat);
    List<Blog> searchByDate(LocalDate startDate, LocalDate endDate);
}
