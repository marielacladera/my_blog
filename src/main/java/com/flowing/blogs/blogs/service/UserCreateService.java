package com.flowing.blogs.blogs.service;

import com.flowing.blogs.blogs.builders.IUserBuilder;
import com.flowing.blogs.blogs.domain.User;
import com.flowing.blogs.blogs.mapper.MapUser;
import com.flowing.blogs.blogs.request.UserCreateRequest;
import com.flowing.blogs.blogs.response.UserResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserCreateService implements IUserBuilder {

    @Autowired
    private IUserService iUserService;

    private UserCreateRequest userCreateRequest;

    private static final MapUser mapper = new MapUser();

    public UserResponse createUser() throws Exception {
        userCreateRequest.setPassword(passwordEncoder().encode(userCreateRequest.getPassword()));
        User userMapper = composeCreateUser(userCreateRequest);
        User user = iUserService.toRegister(userMapper);

        return mapper.mapUserResponse(user);
    }

    public void setUserCreateRequest(UserCreateRequest userCreateRequest) {
        this.userCreateRequest = userCreateRequest;
    }

   @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
