package com.flowing.blogs.blogs.service;

import com.flowing.blogs.blogs.domain.Blog;
import com.flowing.blogs.blogs.domain.Category;
import com.flowing.blogs.blogs.domain.User;
import com.flowing.blogs.blogs.mapper.MapBlog;
import com.flowing.blogs.blogs.response.BlogResponse;
import com.flowing.blogs.blogs.utils.FileFunctions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.flowing.blogs.blogs.builders.IBlogBuilder;
import org.springframework.web.multipart.MultipartFile;

@Service
public class BlogCreateService implements IBlogBuilder {

    @Autowired
    private IBlogService iBlogService;

    @Autowired
    private ICategoryService serviceCategory;

    @Autowired
    private IUserService serviceUser;

    private static final MapBlog mapper = new MapBlog();

    public BlogResponse createBlog(Integer userId, Integer categoryId, String title, String description,
                                   MultipartFile fileInput, MultipartFile imgFileInput) throws Exception {
        Category categoryFound = serviceCategory.toListForId(categoryId);
        User userFound = serviceUser.toListForId(userId);
        if((categoryFound == null && userFound == null) || !(FileFunctions.isFile(fileInput.getOriginalFilename(), "html"))
                || !(FileFunctions.isFile(imgFileInput.getOriginalFilename(), "JPG"))) {
            return null;
        }
        String fileName = FileFunctions.uploadFile(fileInput);
        String imgFileName = FileFunctions.uploadFile(imgFileInput);
        Blog composeBlog = composeCreateBlog(userFound, categoryFound, title, description, fileName, imgFileName);
        Blog blogRegister = iBlogService.toRegister(composeBlog);
        BlogResponse response = mapper.mapperBlogResponse(blogRegister);
        return response;
    }

}
