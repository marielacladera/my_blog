package com.flowing.blogs.blogs.service.impl;

import com.flowing.blogs.blogs.domain.User;
import com.flowing.blogs.blogs.repository.IGenericRepo;
import com.flowing.blogs.blogs.repository.IUserRepo;
import com.flowing.blogs.blogs.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserImpl extends CRUDImpl<User, Integer> implements IUserService, UserDetailsService {
    @Autowired
    private IUserRepo repo;

    @Override
    protected IGenericRepo<User, Integer> getRepo() {
        return repo;
    }

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {

       User userFound = repo.findOneByUserName(userName);

        if(userFound == null) {
            throw new UsernameNotFoundException(String.format("USER NOT FOUND", userName));
        }

        List<GrantedAuthority> roles = new ArrayList<>();
        userFound.getRoles().forEach(rol -> {
            roles.add(new SimpleGrantedAuthority(rol.getName().toString()));
        });

        UserDetails user = new org.springframework.security.core.userdetails.User(userFound.getUserName(), userFound.getPassword(), userFound.isEnabled(), true, true, true, roles);

        return user;
    }
}
