package com.flowing.blogs.blogs.service;

import com.flowing.blogs.blogs.builders.IBlogBuilder;
import com.flowing.blogs.blogs.domain.Blog;
import com.flowing.blogs.blogs.domain.Category;
import com.flowing.blogs.blogs.mapper.MapBlog;
import com.flowing.blogs.blogs.response.BlogResponse;
import com.flowing.blogs.blogs.utils.FileFunctions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class BlogUpdateService implements IBlogBuilder {
    @Autowired
    private IBlogService iBlogService;

    @Autowired
    private ICategoryService serviceCategory;

    @Autowired
    private IUserService serviceUser;

    private static final MapBlog mapper = new MapBlog();

    public BlogResponse updateBlog(Integer blogId, Integer categoryId, String title,
                                                   String description, MultipartFile fileInput,
                                                   MultipartFile imgFileInput, Blog blogFound) throws Exception {
        Category categoryFound = serviceCategory.toListForId(categoryId);
        if (categoryFound == null || !(FileFunctions.isFile(fileInput.getOriginalFilename(), "html")) || !(FileFunctions.isFile(imgFileInput.getOriginalFilename(), "JPG"))) {
            return null;
        }
        FileFunctions.modifyFile(fileInput, blogFound.getNameFile());
        FileFunctions.modifyFile(imgFileInput, blogFound.getNameImage());
        Blog composeBlog = composeUpdateBlog(blogId, blogFound.getUser(), categoryFound, title, description, blogFound.getNameFile(), blogFound.getNameImage());
        Blog blogUpdate = iBlogService.toRegister(composeBlog);
        BlogResponse blogResponse = mapper.mapperBlogResponse(blogUpdate);
        return blogResponse;
    }

}