package com.flowing.blogs.blogs.service.impl;

import com.flowing.blogs.blogs.domain.Rol;
import com.flowing.blogs.blogs.repository.IGenericRepo;
import com.flowing.blogs.blogs.repository.IRolRepo;
import com.flowing.blogs.blogs.service.IRolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RolImpl extends CRUDImpl<Rol, Integer> implements IRolService {
    @Autowired
    private IRolRepo repo;

    @Override
    protected IGenericRepo<Rol, Integer> getRepo() {
        return repo;
    }
}
