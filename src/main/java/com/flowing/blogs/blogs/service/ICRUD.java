package com.flowing.blogs.blogs.service;

import java.util.List;

public interface ICRUD <T, ID>{
    T toRegister(T t) throws Exception;
    List<T> toList() throws Exception;
    T toListForId(ID id) throws Exception;
    T toUpdate(T t) throws Exception;
    void toDelete(ID id) throws Exception;
}
