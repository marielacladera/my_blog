package com.flowing.blogs.blogs.service.impl;

import com.flowing.blogs.blogs.domain.Blog;
import com.flowing.blogs.blogs.repository.IBlogRepo;
import com.flowing.blogs.blogs.repository.IGenericRepo;
import com.flowing.blogs.blogs.service.IBlogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class BlogImpl extends CRUDImpl<Blog, Integer> implements IBlogService {
    @Autowired
    private IBlogRepo repo;

    @Override
    protected IGenericRepo<Blog, Integer> getRepo() {
        return repo;
    }

    @Override
    public List<Blog> searchByUser(Integer idUser, String userName) {
        return repo.searchByUser(idUser, userName);
    }

    @Override
    public List<Blog> searchByCategory(Integer idCat) {
        return repo.searchByCategory(idCat);
    }

    @Override
    public List<Blog> searchByDate(LocalDate startDate, LocalDate endDate) {
        return repo.searchByDate(startDate, endDate.plusDays(1));
    }
}
