package com.flowing.blogs.blogs.controller;

import com.flowing.blogs.blogs.builders.IBlogBuilder;
import com.flowing.blogs.blogs.domain.Blog;
import com.flowing.blogs.blogs.exception.ModelNotFoundException;
import com.flowing.blogs.blogs.mapper.MapBlog;
import com.flowing.blogs.blogs.response.BlogListResponse;
import com.flowing.blogs.blogs.response.BlogResponse;
import com.flowing.blogs.blogs.request.FilterBlogRequest;
import com.flowing.blogs.blogs.service.*;
import com.flowing.blogs.blogs.utils.FileFunctions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;

@RestController
@Validated
@RequestMapping("/blogs")
@CrossOrigin("*")
public class BlogController implements IBlogBuilder {

    @Autowired
    private IBlogService serviceBlog;

    @Autowired
    private BlogCreateService blogCreateService;

    @Autowired
    private BlogUpdateService blogUpdateService;

    private static final MapBlog mapper = new MapBlog();

    @PostMapping
    public ResponseEntity<BlogResponse> createBlog(@RequestParam Integer userId, @RequestParam Integer categoryId, @RequestParam String title,
                                                   @RequestParam String description, @RequestPart MultipartFile fileInput,
                                                   @RequestParam MultipartFile imgFileInput) throws Exception {

       BlogResponse response = blogCreateService.createBlog(userId, categoryId, title, description, fileInput, imgFileInput);
       if(response == null){
           return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
       }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<BlogListResponse>> toList() throws Exception {
        List<Blog> blogs = serviceBlog.toList();
        List<BlogListResponse> response = mapper.mapperListResponse(blogs);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<BlogResponse> toListForId(@PathVariable("id") Integer idBlog) throws Exception {
        Blog blogFound = serviceBlog.toListForId(idBlog);
        if(blogFound == null){
            throw new ModelNotFoundException("ID NOT FOUND " + idBlog);
        }
        BlogResponse response = mapper.mapperBlogResponse(blogFound);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<BlogResponse> updateBlog(@RequestParam Integer blogId, @RequestParam Integer categoryId, @RequestParam String title,
                                                   @RequestParam String description, @RequestPart @NotNull MultipartFile fileInput,
                                                   @RequestPart @NotNull MultipartFile imgFileInput) throws Exception {

        Blog  blogFound = serviceBlog.toListForId(blogId);
        if(blogFound == null){
            throw new ModelNotFoundException("ID NOT FOUND " + blogId);
        }
        BlogResponse response = blogUpdateService.updateBlog(blogId, categoryId, title, description, fileInput, imgFileInput, blogFound);
        if(response == null){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> toDelete(@PathVariable("id") Integer idBlog) throws Exception {
        Blog blogFound = serviceBlog.toListForId(idBlog);
        if(blogFound == null){
            throw new ModelNotFoundException("ID NOT FOUND " + idBlog);
        }
        FileFunctions.deleteFile(blogFound.getNameFile());
        serviceBlog.toDelete(blogFound.getIdBlog());

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PostMapping("/search/user")
    public ResponseEntity<List<BlogListResponse>> searchByUser(@RequestBody FilterBlogRequest filter) {
        List<Blog> blogs = serviceBlog.searchByUser(filter.getIdUser(), filter.getUserName().toLowerCase());
        List<BlogListResponse> response = mapper.mapperListResponse(blogs);

        return  new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("search/category")
    public ResponseEntity<List<BlogListResponse>> searchByCategory(@RequestParam(value = "categoryId") Integer categoryId) {
        List<Blog> blogs = serviceBlog.searchByCategory(categoryId);
        List<BlogListResponse> response = mapper.mapperListResponse(blogs);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/search/date")
    public ResponseEntity<List<BlogListResponse>> searchByDate(@RequestParam(value = "startDate") String startDate, @RequestParam(value = "endDate") String endDate) {
        List<Blog> blogs = serviceBlog.searchByDate(LocalDate.parse(startDate), LocalDate.parse(endDate));
        List<BlogListResponse> response =mapper.mapperListResponse(blogs);

        return  new ResponseEntity<>(response, HttpStatus.OK);
    }
}
