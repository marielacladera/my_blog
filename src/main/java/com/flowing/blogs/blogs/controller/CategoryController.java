package com.flowing.blogs.blogs.controller;

import com.flowing.blogs.blogs.builders.ICategoryBuilder;
import com.flowing.blogs.blogs.domain.Category;
import com.flowing.blogs.blogs.exception.ModelNotFoundException;
import com.flowing.blogs.blogs.response.CategoryResponse;
import com.flowing.blogs.blogs.mapper.MapCategory;
import com.flowing.blogs.blogs.request.CategoryCreateRequest;
import com.flowing.blogs.blogs.request.CategoryUpdateRequest;
import com.flowing.blogs.blogs.service.ICategoryService;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/categories")
@CrossOrigin("*")
public class CategoryController  implements ICategoryBuilder {

    @Autowired
    private ICategoryService serviceCategory;

    private static final MapCategory mapper = new MapCategory();

    @Operation(summary = "register a category")
    @PostMapping
    public ResponseEntity<CategoryResponse> toRegister(@Valid @RequestBody CategoryCreateRequest request) throws Exception {
        Category categoryComposer = composeCreateCategory(request);
        Category categoryCreated = serviceCategory.toRegister(categoryComposer);
        CategoryResponse response = mapper.mapCategoryResponse(categoryCreated);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<CategoryResponse>> toList() throws Exception {
        List<Category> categories = serviceCategory.toList();
        List<CategoryResponse> response = mapper.mapCategoryList(categories);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<CategoryResponse> toListForId (@PathVariable("id") Integer id) throws Exception {
        Category categoryFound = serviceCategory.toListForId(id);
        if(categoryFound == null){
            throw new ModelNotFoundException("ID NOT FOUND " + id);
        }
        CategoryResponse response = mapper.mapCategoryResponse(categoryFound);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<CategoryResponse> toUpdate(@Valid @RequestBody CategoryUpdateRequest request) throws Exception {
        Category categoryFound =  serviceCategory.toListForId(request.getIdCategory());
        if(categoryFound == null){
            throw new ModelNotFoundException("ID NOT FOUND " + request.getIdCategory());
        }
        Category categoryComposer = composeUpdateCategory(request);
        Category categoryUpdated = serviceCategory.toUpdate(categoryComposer);
        CategoryResponse response = mapper.mapCategoryResponse(categoryUpdated);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> toDelete(@PathVariable("id") Integer id) throws Exception {
        Category categoryFound = serviceCategory.toListForId(id);
        if(categoryFound == null){
            throw new ModelNotFoundException("ID NOT FOUND " + id);
        }
        serviceCategory.toDelete(id);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
