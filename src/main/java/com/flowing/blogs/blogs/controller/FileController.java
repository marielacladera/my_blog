package com.flowing.blogs.blogs.controller;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;

@RestController
@RequestMapping(value = "/files")
@RequestScope
public class FileController {

    @GetMapping(value = "/uploads/{fileName:.*}")
    public ResponseEntity<Resource> verFoto(@PathVariable String fileName) {
        Path fileRoute = Paths.get("files/upload-blogs").resolve(fileName).toAbsolutePath();
        Resource resource = null;

        try {
            resource = new UrlResource(fileRoute.toUri());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        if (!resource.exists() && !resource.isReadable()) {
            throw new RuntimeException("Error no se pudo cargar la imagen" + fileName);
        }
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"");
        headers.add(HttpHeaders.CONTENT_TYPE, "application/octet-stream");
        return new ResponseEntity<Resource>(resource, headers, HttpStatus.OK);
    }
}
