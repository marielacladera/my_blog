package com.flowing.blogs.blogs.controller;

import com.flowing.blogs.blogs.builders.IRolBuilder;
import com.flowing.blogs.blogs.domain.Rol;
import com.flowing.blogs.blogs.exception.ModelNotFoundException;
import com.flowing.blogs.blogs.mapper.MapCategory;
import com.flowing.blogs.blogs.mapper.MapRol;
import com.flowing.blogs.blogs.request.CategoryUpdateRequest;
import com.flowing.blogs.blogs.request.RolCreateRequest;
import com.flowing.blogs.blogs.request.RolUpdateRequest;
import com.flowing.blogs.blogs.response.CategoryResponse;
import com.flowing.blogs.blogs.response.RolResponse;
import com.flowing.blogs.blogs.service.IRolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/roles")
@CrossOrigin("*")
public class RolController implements IRolBuilder {
    @Autowired
    private IRolService serviceRol;

    private static final MapRol mapper = new MapRol();

    @PostMapping
    public ResponseEntity<RolResponse> toRegister(@Valid @RequestBody RolCreateRequest request) throws Exception {
        Rol rolComposer = composeCreateRol(request);
        Rol categoryCreated = serviceRol.toRegister(rolComposer);
        RolResponse response = mapper.mapRolResponse(categoryCreated);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<RolResponse>> toList() throws Exception {
        List<Rol> roles = serviceRol.toList();
        List<RolResponse> response = mapper.mapRolList(roles);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<RolResponse> toListForId (@PathVariable("id") Integer id) throws Exception {
        Rol rolFound = serviceRol.toListForId(id);
        if(rolFound == null){
            throw new ModelNotFoundException("ID NOT FOUND " + id);
        }
        RolResponse response = mapper.mapRolResponse(rolFound);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<RolResponse> toUpdate(@Valid @RequestBody RolUpdateRequest request) throws Exception {
        Rol rolFound =  serviceRol.toListForId(request.getIdRol());
        if(rolFound == null){
            throw new ModelNotFoundException("ID NOT FOUND " + request.getIdRol());
        }
        Rol rolComposer = composeUpdateRol(request, rolFound);
        Rol rolUpdated = serviceRol.toUpdate(rolComposer);
        RolResponse response = mapper.mapRolResponse(rolUpdated);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> toDelete(@PathVariable("id") Integer id) throws Exception {
        Rol rolFound = serviceRol.toListForId(id);
        if(rolFound == null){
            throw new ModelNotFoundException("ID NOT FOUND " + id);
        }
        serviceRol.toDelete(id);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
