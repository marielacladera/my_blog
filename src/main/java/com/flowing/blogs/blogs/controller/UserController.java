package com.flowing.blogs.blogs.controller;

import com.flowing.blogs.blogs.builders.IUserBuilder;
import com.flowing.blogs.blogs.domain.User;
import com.flowing.blogs.blogs.exception.ModelNotFoundException;
import com.flowing.blogs.blogs.mapper.MapUser;
import com.flowing.blogs.blogs.response.UserResponse;
import com.flowing.blogs.blogs.request.UserCreateRequest;
import com.flowing.blogs.blogs.request.UserUpdateRequest;
import com.flowing.blogs.blogs.service.IUserService;
import com.flowing.blogs.blogs.service.UserCreateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/users")
@CrossOrigin("*")
public class UserController implements IUserBuilder {

    @Autowired
    private IUserService serviceUser;

    @Autowired
    private UserCreateService userCreateService;

    private static final MapUser mapper = new MapUser();

    @PostMapping
    public ResponseEntity<UserResponse> toRegister(@Valid @RequestBody UserCreateRequest request) throws Exception {
        userCreateService.setUserCreateRequest(request);
        UserResponse response = userCreateService.createUser();

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<UserResponse>> toList() throws Exception{
        List<User> users = serviceUser.toList();
        List<UserResponse> response = mapper.mapUserList(users);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserResponse> toListForID(@PathVariable("id") Integer id) throws Exception {
        User userFound = serviceUser.toListForId(id);
        if(userFound == null) {
            throw new ModelNotFoundException("ID NOT FOUND " + id);
        }
        UserResponse response = mapper.mapUserResponse(userFound);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<UserResponse> toUpdate(@Valid @RequestBody UserUpdateRequest request) throws Exception {
        User userFound = serviceUser.toListForId(request.getIdUser());
        if(userFound == null){
            throw new ModelNotFoundException("ID NOT FOUND " + request.getIdUser());
        }
        User userMapper = composeUpdateUser(request, userFound);
        User userUpdated = serviceUser.toUpdate(userMapper);
        UserResponse response = mapper.mapUserResponse(userUpdated);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> toDelete(@PathVariable("id") Integer id) throws Exception {
        User userFound = serviceUser.toListForId(id);
        if(userFound == null){
            throw new ModelNotFoundException("ID NOT FOUND " + id);
        }
        serviceUser.toDelete(id);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
