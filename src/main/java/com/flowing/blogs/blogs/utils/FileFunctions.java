package com.flowing.blogs.blogs.utils;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

public class FileFunctions {

    private static final String FILES_ROUTE = "files/upload-blogs";

    public static String uploadFile(MultipartFile fileInput) throws Exception {
        String filename = new String();
        if(!fileInput.isEmpty()){
            String fileNameAux = UUID
                    .randomUUID()
                    .toString()+"-"+fileInput
                    .getOriginalFilename()
                    .replace(" ", "-");
            Path fileRoute = Paths
                    .get(FILES_ROUTE)
                    .resolve(fileNameAux)
                    .toAbsolutePath();
            filename = fileNameAux;
            try{
                Files.copy(fileInput.getInputStream(), fileRoute);
            }catch (IOException e){
                throw new Exception("NOT FOUND FILE" + e);
            }
        }

        return filename;
    }

    public static void modifyFile(MultipartFile fileInput, String filename) throws Exception{
        deleteFile(filename);
        if(!fileInput.isEmpty()){
            Path fileRoute = Paths
                    .get(FILES_ROUTE)
                    .resolve(filename)
                    .toAbsolutePath();
            try{
                Files.copy(fileInput.getInputStream(), fileRoute);
            }catch (IOException e){
                throw new Exception("NOT FOUND FILE" + e);
            }
        }
    }

    public static void deleteFile(String fileName) throws Exception {
        Path fileRoute = Paths.
                get(FILES_ROUTE)
                .resolve(fileName);
        try{
            Files.delete(fileRoute);
        }catch (IOException e){
            throw new Exception("NOT FOUND FILE" + e);
        }
    }

    public static boolean isFile(String fileName, String extension) {
        int indice = fileName.lastIndexOf(".");
        String extFile = fileName.substring(indice+1, fileName.length());

        return extFile.equals(extension);
    }
}
