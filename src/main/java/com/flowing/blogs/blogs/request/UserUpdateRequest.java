package com.flowing.blogs.blogs.request;

import com.flowing.blogs.blogs.domain.Rol;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

public class UserUpdateRequest {
    @Schema(description = "idUser of user")
    @Min(1)
    private Integer idUser;

    @Schema(description = "firstName of user")
    @NotBlank
    @Size(min = 3)
    private String firstName;

    @Schema(description = "lastName of user")
    @NotBlank
    @Size(min = 3)
    private String lastName;

    @Schema(description = "cellPhone of user")
    @Min(4)
    private Integer cellPhone;

    @Schema(description = "roles of user")
    private List<Rol> roles;

    @NotNull
    private boolean enabled;

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getCellPhone() {
        return cellPhone;
    }

    public void setCellPhone(Integer cellPhone) {
        this.cellPhone = cellPhone;
    }

    public List<Rol> getRoles() {
        return roles;
    }

    public void setRoles(List<Rol> roles) {
        this.roles = roles;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
