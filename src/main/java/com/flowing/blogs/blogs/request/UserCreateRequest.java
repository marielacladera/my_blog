package com.flowing.blogs.blogs.request;

import com.flowing.blogs.blogs.domain.Rol;

import javax.validation.constraints.*;
import java.util.List;

import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.security.oauth2.client.test.OAuth2ContextConfiguration;

public class UserCreateRequest {

    @Schema(description = "username of user")
    @NotBlank
    @Size(min = 3)
    private String userName;

    @Schema(description = "firstName of user")
    @NotBlank
    @Size(min = 3)
    private String firstName;

    @Schema(description = "lastName of user")
    @NotBlank
    @Size(min = 3)
    private String lastName;

    @Schema(description = "email of user")
    @NotBlank
    @Email
    private String email;

    @Schema(description = "cellPhone of user")
    @Min(4)
    private Integer cellPhone;

    @Schema(description = "roles of user")
    private List<Rol> roles;

    @Schema(description = "password of user")
    @NotBlank
    private String password;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getCellPhone() {
        return cellPhone;
    }

    public void setCellPhone(Integer cellPhone) {
        this.cellPhone = cellPhone;
    }

    public List<Rol> getRoles() {
        return roles;
    }

    public void setRoles(List<Rol> roles) {
        this.roles = roles;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
