package com.flowing.blogs.blogs.request;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.NotBlank;

public class RolCreateRequest {

    @Schema(description = "name of rol")
    @NotBlank
    private String name;

    @Schema(description = "description of rol")
    @NotBlank
    private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
