package com.flowing.blogs.blogs.request;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Schema(description = "request for creating a category")
public class CategoryCreateRequest {
    @Schema(description = "category name")
    @NotBlank
    @Size(min = 3)
    private String name;

    @Schema(description = "category description")
    @NotBlank
    @Size(min = 3)
    private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
