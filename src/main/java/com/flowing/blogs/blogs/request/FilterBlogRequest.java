package com.flowing.blogs.blogs.request;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.Min;

public class FilterBlogRequest {

    @Schema(description = "idUser of user")
    @Min(1)
    Integer idUser;

    @Schema(description = "userName of user")
    String userName;

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

}
