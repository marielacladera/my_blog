package com.flowing.blogs.blogs.request;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Schema(description = "request for update a category")
public class CategoryUpdateRequest {
    @Schema(description = "id of category")
    @Min(1)
    private Integer idCategory;

    @Schema(description = "category name")
    @NotBlank
    @Size(min = 3)
    private String name;

    @Schema(description = "category description")
    @NotBlank
    @Size(min = 3)
    private  String description;

    public Integer getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(Integer idCategory) {
        this.idCategory = idCategory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
