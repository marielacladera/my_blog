package com.flowing.blogs.blogs.request;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

public class RolUpdateRequest {
    @Schema(description = "idRol of rol")
    @Min(1)
    private Integer idRol;

    @Schema(description = "description of rol")
    @NotBlank
    private String description;

    public Integer getIdRol() {
        return idRol;
    }

    public void setIdRol(Integer idRol) {
        this.idRol = idRol;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
