package com.flowing.blogs.blogs.domain;

import com.flowing.blogs.blogs.domain.enums.ERol;

import javax.persistence.*;

@Entity
@Table(name = "rol")
public class Rol {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer IdRol;

    @Enumerated(EnumType.STRING)
    @Column(name = "rol", unique = true)
    private ERol name;

    @Column(name = "description")
    private String description;

    public Integer getIdRol() {
        return IdRol;
    }

    public void setIdRol(Integer idRol) {
        IdRol = idRol;
    }

    public ERol getName() {
        return name;
    }

    public void setName(ERol name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
