package com.flowing.blogs.blogs.builders;

import com.flowing.blogs.blogs.domain.Blog;
import com.flowing.blogs.blogs.domain.Category;
import com.flowing.blogs.blogs.domain.User;

import java.time.LocalDate;

public interface IBlogBuilder {

    default Blog composeCreateBlog (User user, Category category, String title, String description,
                                    String fileName, String imgFileName) {
        Blog blog = new Blog();
        blog.setUser(user);
        blog.setCategory(category);
        blog.setTitle(title);
        blog.setDescription(description);
        blog.setNameFile(fileName);
        blog.setPublicationDate(LocalDate.now());
        blog.setNameImage(imgFileName);

        return blog;
    }

    default Blog composeUpdateBlog (Integer idBlog, User user, Category category, String title, String description,
                                    String fileName, String imgFileName) {
        Blog blog = new Blog();
        blog.setIdBlog(idBlog);
        blog.setUser(user);
        blog.setCategory(category);
        blog.setTitle(title);
        blog.setDescription(description);
        blog.setNameFile(fileName);
        blog.setNameImage(imgFileName);
        blog.setPublicationDate(LocalDate.now());

        return blog;
    }
}
