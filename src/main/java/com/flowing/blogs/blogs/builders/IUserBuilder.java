package com.flowing.blogs.blogs.builders;

import com.flowing.blogs.blogs.domain.User;
import com.flowing.blogs.blogs.request.UserCreateRequest;
import com.flowing.blogs.blogs.request.UserUpdateRequest;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public interface IUserBuilder {

    default User composeCreateUser(UserCreateRequest request){
        User userBuilder = new User();
        userBuilder.setFirstName(request.getFirstName());
        userBuilder.setLastName(request.getLastName());
        userBuilder.setUserName(request.getUserName());
        userBuilder.setEmail(request.getEmail());
        userBuilder.setCellPhone(request.getCellPhone());
        userBuilder.setRoles(request.getRoles());
        userBuilder.setEnabled(true);
        userBuilder.setPassword(request.getPassword());

        return userBuilder;
    }

    default User composeUpdateUser(UserUpdateRequest request, User user){
        User userBuilder = new User();
        userBuilder.setIdUser(user.getIdUser());
        userBuilder.setFirstName(request.getFirstName());
        userBuilder.setLastName(request.getLastName());
        userBuilder.setUserName(user.getUserName());
        userBuilder.setEmail(user.getEmail());
        userBuilder.setCellPhone(request.getCellPhone());
        userBuilder.setRoles(request.getRoles());
        userBuilder.setEnabled(request.isEnabled());

        return userBuilder;
    }
}
