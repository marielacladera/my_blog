package com.flowing.blogs.blogs.builders;

import com.flowing.blogs.blogs.domain.Rol;
import com.flowing.blogs.blogs.domain.enums.ERol;
import com.flowing.blogs.blogs.request.RolCreateRequest;
import com.flowing.blogs.blogs.request.RolUpdateRequest;

public interface IRolBuilder {
    default Rol composeCreateRol(RolCreateRequest request) {
        String name = request.getName().toUpperCase();
        ERol rolName = ERol.valueOf(name);
        Rol rol = new Rol();
        rol.setName(rolName);
        rol.setDescription(request.getDescription());
        return rol;
    }

    default Rol composeUpdateRol(RolUpdateRequest request, Rol rolFound) {
        Rol rol = new Rol();
        rol.setIdRol(rolFound.getIdRol());
        rol.setName(rolFound.getName());
        rol.setDescription(request.getDescription());
        return rol;
    }
}
