package com.flowing.blogs.blogs.builders;

import com.flowing.blogs.blogs.domain.Category;
import com.flowing.blogs.blogs.request.CategoryCreateRequest;
import com.flowing.blogs.blogs.request.CategoryUpdateRequest;

public interface ICategoryBuilder {

    default Category composeCreateCategory(CategoryCreateRequest request) {
        Category categoryBuilder = new Category();
        categoryBuilder.setName(request.getName());
        categoryBuilder.setDescription(request.getDescription());

        return categoryBuilder;
    }

    default Category composeUpdateCategory(CategoryUpdateRequest request) {
        Category categoryBuilder = new Category();
        categoryBuilder.setIdCategory(request.getIdCategory());
        categoryBuilder.setName(request.getName());
        categoryBuilder.setDescription(request.getDescription());
        return categoryBuilder;
    }
}
