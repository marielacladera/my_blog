package com.flowing.blogs.blogs.mapper;

import com.flowing.blogs.blogs.domain.Category;
import com.flowing.blogs.blogs.response.CategoryResponse;

import java.util.ArrayList;
import java.util.List;

public class MapCategory {

    public CategoryResponse mapCategoryResponse(Category category) {
        CategoryResponse categoryResponse = new CategoryResponse();
        categoryResponse.setIdCategory(category.getIdCategory());
        categoryResponse.setName(category.getName());
        categoryResponse.setDescription(category.getDescription());

        return categoryResponse;
    }

    public ArrayList<CategoryResponse> mapCategoryList(List<Category> listCategory) {
        ArrayList<CategoryResponse> listCategoryResponse = new ArrayList<CategoryResponse>();
        for(Category category: listCategory){
            CategoryResponse categoryResponse = mapCategoryResponse(category);
            listCategoryResponse.add(categoryResponse);
        }

        return listCategoryResponse;
    }
}
