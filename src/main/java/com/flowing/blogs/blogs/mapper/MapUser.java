package com.flowing.blogs.blogs.mapper;

import com.flowing.blogs.blogs.domain.User;
import com.flowing.blogs.blogs.response.UserResponse;

import java.util.ArrayList;
import java.util.List;

public class MapUser {

    public UserResponse mapUserResponse(User user) {
        UserResponse userResponse = new UserResponse();
        userResponse.setIdUser(user.getIdUser());
        userResponse.setUserName(user.getUserName());
        userResponse.setFirstName(user.getFirstName());
        userResponse.setLastName(user.getLastName());
        userResponse.setEmail(user.getEmail());
        userResponse.setCellPhone(user.getCellPhone());
        userResponse.setRoles(user.getRoles());

        return userResponse;
    }

    public List<UserResponse> mapUserList (List<User> listUser) {
        List<UserResponse> listUserResponse = new ArrayList<UserResponse>();
        for(User user: listUser){
            UserResponse userResponse = mapUserResponse(user);
            listUserResponse.add(userResponse);
        }

        return listUserResponse;
    }
}
