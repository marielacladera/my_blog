package com.flowing.blogs.blogs.mapper;

import com.flowing.blogs.blogs.domain.Rol;
import com.flowing.blogs.blogs.response.CategoryResponse;
import com.flowing.blogs.blogs.response.RolResponse;

import java.util.ArrayList;
import java.util.List;


public class MapRol {

    public RolResponse mapRolResponse(Rol rol) {
        RolResponse rolResponse = new RolResponse();
        rolResponse.setIdRol(rol.getIdRol());
        rolResponse.setName(rol.getName());
        rolResponse.setDescripcion(rol.getDescription());

        return rolResponse;
    }

    public ArrayList<RolResponse> mapRolList(List<Rol> listRol) {
        ArrayList<RolResponse> listRolResponse = new ArrayList<RolResponse>();
        for(Rol rol: listRol){
            RolResponse rolResponse = mapRolResponse(rol);
            listRolResponse.add(rolResponse);
        }

        return listRolResponse;
    }
}
