package com.flowing.blogs.blogs.mapper;

import com.flowing.blogs.blogs.domain.Blog;
import com.flowing.blogs.blogs.response.BlogListResponse;
import com.flowing.blogs.blogs.response.BlogResponse;

import java.util.ArrayList;
import java.util.List;

public class MapBlog {

   public BlogResponse mapperBlogResponse(Blog blog) {
        BlogResponse blogResponse = new BlogResponse();
        blogResponse.setIdBlog(blog.getIdBlog());
        blogResponse.setUser(blog.getUser());
        blogResponse.setCategory(blog.getCategory());
        blogResponse.setTitle(blog.getTitle());
        blogResponse.setDescription(blog.getDescription());
        blogResponse.setPublicationDate(blog.getPublicationDate());
        blogResponse.setNameFile(blog.getNameFile());
        blogResponse.setNameImage(blog.getNameImage());

        return blogResponse;
   }

   public BlogListResponse mapperBlogListResponse(Blog blog) {
       BlogListResponse blogResponse = new BlogListResponse();
       blogResponse.setIdBlog(blog.getIdBlog());
       blogResponse.setTitle(blog.getTitle());
       blogResponse.setDescription(blog.getDescription());
       blogResponse.setNameImage(blog.getNameImage());

       return blogResponse;
   }

    public List<BlogListResponse> mapperListResponse (List<Blog> listBlog) {
        List<BlogListResponse> listBlogResponse = new ArrayList<BlogListResponse>();
        for(Blog blog : listBlog){
            BlogListResponse blogResponse = mapperBlogListResponse(blog);
            listBlogResponse.add(blogResponse);
        }

        return listBlogResponse;
    }

}
